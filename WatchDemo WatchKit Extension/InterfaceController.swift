//
//  InterfaceController.swift
//  WatchDemo WatchKit Extension
//
//  Created by Simara on 28/10/19.
//  Copyright © 2019 Simara. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

     var labelSaysHello:Bool = false
    
    @IBOutlet weak var watchLbl: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func watchBtnPressed() {
        self.watchLbl.setText("Button pressed!")
        print("Button pressed!")
        
        if (labelSaysHello == true) {
            watchLbl.setText("GOODBYE")
            self.labelSaysHello = false
        }
        else {
            watchLbl.setText("HELLO")
            self.labelSaysHello = true
        }
    }
}
