//
//  ViewController.swift
//  WatchDemo
//
//  Created by Simara on 28/10/19.
//  Copyright © 2019 Simara. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    
    // Protocol Functions for WCSessionDelegate
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    @IBOutlet weak var phoneLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("checking to see if wc session is supported")
        
        // Check if your IOS version can talk to the phone
        // Does your iPhone support "talking to a watch"?
        // If yes, then create the session
        // If no, then output error message
        if (WCSession.isSupported()) {
            print("PHONE: Phone supports WatchConnectivity!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("PHONE: Phone does not support WatchConnectivity")
        }

    }

    @IBAction func sendBtn(_ sender: UIButton) {
        
        self.phoneLbl.text = "Button pressed!"
    }
    
}

